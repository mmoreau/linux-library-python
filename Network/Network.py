#!/usr/bin/python3

import os
from pprint import pprint

class String:

	@classmethod
	def trim(cls, string: str, pattern: (tuple, list) = ("\n", "\t", "\r", "\0", "\x0b", " ")) -> str:

		"""
			Lets you delete spaces and other characters without duplicating the character string. 
		    This function can be compared to the trim function of PHP.
		"""
        
		count = 0
		switch = False

		try:
			for character in enumerate(string):
				if character[1] in pattern:
					if switch:
						if string[(character[0] + 1)] not in pattern:
							string = f"{string[0:count]}{character[1]}{string[count + 1:]}"
							count += 1
				else:
					switch = True
					string = f"{string[0:count]}{character[1]}{string[count + 1:]}"
					count += 1
		except:
			pass

		return string[:count]



class Network:


    protocols = ("tcp", "udp", "tcp6", "udp6")

    ipv4_private = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]


    @classmethod
    def ipv4_repr2addr(cls, r: str) -> str:

        """Convert an IPV4 representation to an IPV4 address."""
        
        return f'{f"{int(r[6:8], 16)}"}.{f"{int(r[4:6], 16)}"}.{f"{int(r[2:4], 16)}"}.{f"{int(r[0:2], 16)}"}' if isinstance(r, str) and len(r) == 8 else ""



    @classmethod
    def ipv4_ptr(cls, address: str):

        """Reverse IPv4 lookup."""

        return f"{'.'.join(address.split('.')[::-1])}.in-addr.arpa" if isinstance(address, str) else ""



    @classmethod
    def ipv4_subnet_address(cls, address: str, subnet: str):

        """Calculate subnet address."""

        return ".".join([f"{int(a[0]) & int(a[1])}" for a in list(zip(address.split("."), subnet.split(".")))]) if isinstance(address, str) and isinstance(subnet, str) else ""



    @classmethod
    def ipv4_host_address(cls, address: str, subnet_address: str) -> str:

        """Calculate host address."""

        return ".".join([f"{int(a[0]) ^ int(a[1])}" for a in list(zip(address.split("."), subnet_address.split(".")))]) if isinstance(address, str) and isinstance(subnet_address, str) else ""



    @classmethod
    def ipv4_2_ipv6_short(cls, address: str) -> str:

        """Convert an IPv4 address to a short IPv6 address."""

        return [f"::ffff:{a[:4]}:{a[4:]}" for a in [Network.ipv4_2_ipv6_hex(address)]][0] if isinstance(address, str) else ""



    @classmethod
    def ipv4_2_ipv6_long(cls, address: str) -> str:

        """Convert an IPv4 address to a long IPv6 address."""

        return f"0000:0000:0000:0000:0000{Network.ipv4_2_ipv6_short(address)[1:]}" if isinstance(address, str) else ""



    @classmethod
    def ipv4_2_ipv6_hex(cls, address: str) -> str:

        """Convert an IPv4 address to an IPv6 address represented as hexadecimal."""

        return "".join([hex(int(a))[2:].zfill(2) for a in address.split(".")]) if isinstance(address, str) else ""



    @classmethod
    def ipv6_repr2addr(cls, r: str) -> str:

        """Convert an IPv6 representation to an IPv6 address."""

        return f'{f"{r[6]}{r[7]}{r[4]}{r[5]}"}:{f"{r[2]}{r[3]}{r[0]}{r[1]}"}:{f"{r[14]}{r[15]}{r[12]}{r[13]}"}:{f"{r[10]}{r[11]}{r[8]}{r[9]}"}:{f"{r[22]}{r[23]}{r[20]}{r[21]}"}:{f"{r[18]}{r[19]}{r[16]}{r[17]}"}:{f"{r[30]}{r[31]}{r[28]}{r[29]}"}:{f"{r[26]}{r[27]}{r[24]}{r[25]}"}' if isinstance(r, str) and len(r) == 32 else ""
    


    @classmethod
    def ipv6_ptr(cls, address: str) -> str:

        """Reverse IPv6 lookup.""" 

        if isinstance(address, str):
            if 0 < len(address) < 40:

                address = address.split(":")
                pos = address.index('')
                address = [data[1].zfill(4) for data in enumerate(address)]
                address[pos] += "0000" * (8 - len(address))

                address = "".join(address)[::-1]
                address = "".join([data[1].ljust(2, ".") for data in enumerate(address)])

                return f"{address}ip6.arpa"

        return ""



    @classmethod
    def ipv6_optimize(cls, address: str) -> str:
        
        """Optimize an IPV6 address."""

        if isinstance(address, str):
            if 0 < len(address) < 40:

                address = address.split(":")

                # 1st Optimization
                info = (None, None)
                count = 0

                for data in enumerate(address):
                    
                    dataStrip = data[1].lstrip("0")

                    if dataStrip:
                        count = 0
                    else:
                        count += 1

                        if info[0] is not None:
                            if count > info[0]:
                                info = count, data[0]
                        else:
                            info = count, data[0]

                    address[data[0]] = dataStrip if dataStrip else "0"
                

                # 2nd Optimization
                if info is not (None, None): # if info:
                    address = f"{':'.join(address[0:(info[1] - info[0]) + 1])}{(('0:' if info[1] == 0 else '::'))}{(':'.join(address[info[1] + 1:]))}"
                    address = "::" if address in ("::", "0:") else address
                else:
                    address = ":".join(address)
                    address = f"::{address}" if address == "1" else address

                return address
        return ""

        

    @classmethod
    def connections(cls, protocol: (str, list, tuple)="tcp4") -> dict:

        """Retrieves connections from Linux files according to the function setting."""

        lock = False

        if isinstance(protocol, str):
            protocol = [protocol]
            lock = True
        elif isinstance(protocol, (list, tuple)):
            lock = True

        connections_data = {}

        if lock:
            objfunc = Network.__dict__

            for p in protocol:
                path = f"/proc/net/{p}"
                if os.path.exists(path):
                    with open(path, "r") as f:
                        protocol_version = bool(p[3:4])
                        #print(p[:3].upper(), (":", "6 :")[protocol_version])
                        connections_data[p] = []
                        objcls = objfunc[("ipv4_repr2addr", "ipv6_repr2addr")[protocol_version]]
                        for line in f.readlines()[1:]:
                            try:
                                lineSplit = String.trim(line).split(" ")
                                local, remote = lineSplit[1].split(":"), lineSplit[2].split(":")
                                local = objcls.__func__(None, local[0]), int(local[1], 16)
                                remote = objcls.__func__(None, remote[0]), int(remote[-1], 16)

                                if protocol_version:
                                    connections_data[p].append((Network.ipv6_optimize(local[0]), local[1], Network.ipv6_optimize(remote[0]), remote[1]))
                                    #print(f"\t[{Network.ipv6_optimize(local[0])}]:{local[1]} => [{Network.ipv6_optimize(remote[0])}]:{remote[1]}")
                                else:
                                    #print(f"\t{local[0]: >15}:{local[1]: <5} => {remote[0]}:{remote[1]}")
                                    connections_data[p].append((local[0], local[1], remote[0], remote[1]))
                            except:
                                pass

        return connections_data


pprint(Network.connections(Network.protocols))
#pprint(Network.connections(("udp", "tcp")))
#pprint(Network.connections("tcp"))

#print(Network.ipv4_ptr("192.168.1.3"))
#print(Network.ipv6_ptr("FE80::8420:8B86:325F:EAF1"))

#print(Network.ipv4_subnet_address("91.198.174.2", "255.255.224.0"))
#print(Network.ipv4_host_address("192.168.1.20", Network.ipv4_subnet_address("192.168.1.20", "255.255.225.0")))

#print(Network.ipv4_2_ipv6_short("192.168.1.40"))
#print(Network.ipv4_2_ipv6_hex("192.168.1.40"))
#print(Network.ipv4_2_ipv6_long("192.168.1.40"))