#!/usr/bin/python3

import os
import re

class Process:

    @classmethod
    def fd(cls, pid):
        
        path = f"/proc/{pid}/fd/"

        if os.path.exists(path):
            if os.access(path, os.X_OK):
                for l in os.listdir(path):
                    realpath = os.path.realpath(path + l)

                    if re.search("socket|pipe|eventfd|eventpoll|deleted", realpath) is None:
                        print(realpath)



    @classmethod
    def cmd(cls, pid):

        path = f"/proc/{pid}/cmdline"

        if os.path.exists(path):
            if os.access(path, os.R_OK):
                with open(path, "r") as f:
                    print(f.readline())

    

    @classmethod
    def exe(cls, pid):

        path = f"/proc/{pid}/exe"

        if os.path.exists(path):
            if os.access(path, os.R_OK):
                print(os.path.realpath(path))



    @classmethod
    def comm(cls, pid):

        path = f"/proc/{pid}/comm"

        if os.path.exists(path):
            if os.access(path, os.R_OK):
                with open(path, "r") as f:
                    print(f.readline().split("\n")[0])



#Process.fd(6407)
#Process.cmd(6707)
#Process.exe(6407)  
#Process.comm(6407)